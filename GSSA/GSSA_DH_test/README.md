This is a quick guide to import data into OpenLog QGIS plugin.

# Data

3 folders are available :

- `DTM` : a DTM located on data. Can be used for automatic elevation definition.
- `sets` : dataset for several collars. There is no imagery available.
- `singles` : dataset for collar with hole_id 288777 . Imagery is available.

For data there are 2 folders :

- 3_files : contains data for collar, survey and lithology definition
- assays : contains data for assays definition

# 3 files

## Collar

Collar must be imported from `drillhole_details.csv`.

CRS is  `EPSG:7853 - GDA2020 / MGA zone 53`.

Date format is `%d/%m/%Y`.

Column definition mapping is:

- __HoleID__ : `DRILLHOLE_NO`
- __Easting__ : `EASTING_GDA2020`
- __Northing__ : `NORTHING_GDA2020`
- __Elevation__ : `ELEVATION_M` (Can also be retrieve from DTM)
- __End of Hole__ : `MAX_DRILLED_DEPTH` (optionnal)
- __Survey Date__ : `MAX_DRILLED_DEPTH_DATE` (optionnal)

## Survey

Survey must be imported from `drillhole_details.csv`.

*Dips must be inverted*

Column definition mapping is:

- __HoleID__ : `DRILLHOLE_NO`
- __Dip__ : `INCLINATION`
- __Azimuth__ : `AZIMUTH`
- __Lenght__ : `MAX_DRILLED_DEPTH`

__Imported survey are linear, only one lenght is defined for these collars__

## Lithology

Lithology must be imported from `litho_details.csv`.

Column definition mapping is:

- __HoleID__ : `DRILLHOLE_NO`
- __LithCode__ : `MAJOR_LITH_CODE`
- __From_m__ : `DEPTH_FROM_M`
- __To_m__ : `DEPTH_TO_M`
