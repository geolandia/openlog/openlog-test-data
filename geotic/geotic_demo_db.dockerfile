FROM mcr.microsoft.com/mssql/server

# Note: This isn't a secure password, and please don't use this for production.
ENV MSSQL_SA_PASSWORD='YourStrong!Password'
ENV ACCEPT_EULA=Y

# Setting the user
USER mssql

COPY data/DemoMine_820.bak DemoMine_820.bak

# Launch SQL Server, confirm startup is complete, restore the database, then terminate SQL Server.
RUN ( /opt/mssql/bin/sqlservr & ) | grep -q "Service Broker manager has started" \
    && /opt/mssql-tools/bin/sqlcmd  -S localhost -U sa -P $MSSQL_SA_PASSWORD -Q 'RESTORE DATABASE GeoticDb FROM DISK= "/DemoMine_820.bak" WITH MOVE "DemoMine" TO "/var/opt/mssql/data/DemoMine.mdf", MOVE "DemoMine_Log" TO "/var/opt/mssql/data/DemoMine.ldf"' \
    && pkill sqlservr

CMD ["/opt/mssql/bin/sqlservr"]