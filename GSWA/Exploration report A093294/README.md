Dataset contributed by:

- Grant Boxer, boxerg@iinet.net.au, https://www.linkedin.com/in/grant-boxer-89117918/

Sourced from:

- The Geological Survey of Western Australia (GSWA) Company Mineral Drillhole Database (MDHDB), [Nullagine sector](https://geoview.dmp.wa.gov.au/GeoView/?viewer=GeoVIEW&layerTheme=8&scale=36111.909643&basemap=&center=120.12311118306317%2C-21.57700596747344&layers=2oedBf1Im0SM26659024GOYc32djAw1S%2FlSm2WzLz41vOPyA0tltab0cWHJt10BJqF1fuIoj2UWxDA2dy0to2FrZvC32YOHP2rBYSj3kNUlL0lojz92aL%2FTH1MiVK%2B1p2oSX0IOuTD3va3T63N3E3W3ep0KX3aGYDA2NwdwN3sUdMW3VXBOB01Ua%2F%2B0MW85d1Pocd11Pocd11Pocd11Pocd11Pocd11Pocd11UWjnc1zepqy0mGuPF0VzRvm1mVnS81Uqp2O0FylWn2zjBxC&extent=120.05823638802778%2C-21.60785371769557%2C120.18798597809857%2C-21.54615821725131)
- The Geological Survey of Western Australia (GSWA) Mineral exploration report (WAMEX) [A093294](https://geodocs.dmirs.wa.gov.au/Web/documentlist/10/Report_Ref/A93294)

Licensed under:
- See [Licence_CCBY4.pdf](Licence_CCBY4.pdf) file